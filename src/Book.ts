import { Item } from './Item';
import { Pages } from './Pages';

export class Book extends Item {
  public get author(): string {
    return this._author;
  }
  public set author(value: string) {
    this._author = value;
  }
  constructor(_title: string, private _author: string, _pages: Pages) {
    super(_title, _pages);
  }
  toString(): string {
    // return `Book: ${this._title} by ${this._author} with number of pages: ${this._pages.length}`;

    return '';
  }
}
