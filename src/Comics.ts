import { Item } from './Item';
import { Pages } from './Pages';

export class Comics extends Item {
  public get artist(): string {
    return this._artist;
  }
  public set artist(value: string) {
    this._artist = value;
  }
  public get author(): string {
    return this._author;
  }
  public set author(value: string) {
    this._author = value;
  }

  constructor(title: string, private _artist: string, private _author: string, pages: Pages) {
    super(title, pages);
  }

  toString(): string {
    // return `Comics: ${this._title} by ${this._author}, the artist is ${this._artist}, number of pages: ${this._pages.length}`;

    return '';
  }
}
