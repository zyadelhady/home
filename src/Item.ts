import { Pages } from './Pages';

export abstract class Item {
  public get title(): string {
    return this._title;
  }
  public set title(value: string) {
    this._title = value;
  }
  constructor(protected _title: string, protected _pages: Pages) {}
  abstract toString(): string;

  public *[Symbol.iterator]() {
    const ipages = this._pages.PagesArr[Symbol.iterator];
    return ipages;
  }
}
