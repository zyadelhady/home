import { Item } from './Item';
import { Pages } from './Pages';

export class Magazine extends Item {
  constructor(_title: string, pages: Pages) {
    super(_title, pages);
  }

  toString(): string {
    // return `Magazine: ${this._title} with number of pages: ${this._pages.length}`;

    return '';
  }
}
