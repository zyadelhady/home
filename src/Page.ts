export enum PageType {
  'with text' = 'with text',
  'with article' = 'with article',
  'with images' = 'with images',
}

export enum MaterialType {
  'simple paper' = 'simple paper',
  'glossy paper' = 'glossy paper',
}

export class Page {
  constructor(private _pageNumber: number, private _pageType: PageType, private _pageMaterial: MaterialType) {}
}
