import { Book } from './Book';
import { Magazine } from './Magazine';
import { MaterialType, Page, PageType } from './Page';
import { Pages } from './Pages';

const book1 = new Book('zyad', 'zizo', new Pages([new Page(1, PageType['with text'], MaterialType['glossy paper'])]));

const magazine1 = new Magazine('zyad', new Pages([new Page(1, PageType['with text'], MaterialType['glossy paper'])]));

console.log(new Page(1, PageType['with article'], MaterialType['glossy paper']));

for (const page of book1) {
  console.log(page);
}
