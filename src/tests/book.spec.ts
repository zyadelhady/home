import { Book } from '../Book';
import { MaterialType, Page, PageType } from '../Page';
import { Pages } from '../Pages';

describe('Book', () => {
  it('toString should return correct value', () => {
    let counter = 1;
    const book = new Book(
      'Harry Potter',
      'J. K. Rowling',
      new Pages([
        new Page(1, PageType['with text'], MaterialType['simple paper']),
        new Page(2, PageType['with text'], MaterialType['simple paper']),
        new Page(3, PageType['with text'], MaterialType['simple paper']),
        new Page(4, PageType['with text'], MaterialType['simple paper']),
      ])
    );

    // for (const page of book) {
    //   expect(page.toString()).toEqual(
    //     `Book: Harry Potter by J. K. Rowling with number of pages: 4, here is page with text #${counter} and it\'s material is simple paper`
    //   );
    //   counter++;
    // }
  });
});
