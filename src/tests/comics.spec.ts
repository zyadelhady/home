import { Comics } from '../Comics';
import { MaterialType, Page, PageType } from '../Page';
import { Pages } from '../Pages';

describe('Comics', () => {
  it('toString should return correct value', () => {
    let counter = 1;
    const comics = new Comics(
      'Spider-Man',
      'Stan Lee',
      'some author',
      new Pages([
        new Page(1, PageType['with images'], MaterialType['glossy paper']),
        new Page(2, PageType['with images'], MaterialType['glossy paper']),
      ])
    );

    // for (const page of comics) {
    //   expect(page.toString()).toEqual(
    //     `Comics: Spider-Man by Stan Lee, the artist is some author, number of pages: 2, here is page with images #${counter} and it\'s material is glossy paper`
    //   );
    //   counter++;
    // }
  });
});
