import { Magazine } from '../Magazine';
import { MaterialType, Page, PageType } from '../Page';
import { Pages } from '../Pages';

describe('Magazine', () => {
  it('toString should return correct value', () => {
    let counter = 1;
    const magazine = new Magazine(
      'G.Q',
      new Pages([
        new Page(1, PageType['with article'], MaterialType['glossy paper']),
        new Page(2, PageType['with article'], MaterialType['glossy paper']),
      ])
    );

    // for (const page of magazine) {
    //   expect(page.toString()).toEqual(
    //     `Magazine: G.Q with number of pages: 2, here is page with article #${counter} and it\'s material is glossy paper`
    //   );
    //   counter++;
    // }
  });
});
