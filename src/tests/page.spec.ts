import { MaterialType, Page, PageType } from '../Page';

describe('Page', () => {
  it('toString should return correct value', () => {
    const page = new Page(3, PageType['with text'], MaterialType['simple paper']);

    expect(page.toString()).toEqual("here is page with text #3 and it's material is paper");
  });
});
